import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class Database {

	private static final String CREATETABLE = "CREATE TABLE Sessions (Spieler varchar(100), Sessionnumber int, Punkte int, CONSTRAINT PK_Sessions PRIMARY KEY (Spieler, Sessionnumber))";
	private static final String CREATETABLE_Gesamt = "CREATE TABLE Gesamt (Spieler varchar(100), Punkte int, CONSTRAINT PK_Gesamt PRIMARY KEY (Spieler));";
	private static final String INSERT_TEMPLATE_Sessions = "INSERT INTO Sessions (Spieler, Sessionnumber, Punkte) VALUES ('%1s', %2d, %3d );";
	private static final String INSERT_TEMPLATE_Gesamt = "INSERT INTO Gesamt (Spieler, Punkte) VALUES ('%1s', %2d );";
	private static final String Update_TEMPLATE_Gesamt = "Update Gesamt set Punkte = %1d where Spieler = '%2s';";
	private static final String SELECT_TEMPLATE_Gesamt = "Select Punkte from Gesamt where Spieler = '%1s';";
	private static final String SELECT_BYPLAYERANDSESSION = "SELECT Spieler, SUM(Punkte) AS Punkte FROM Sessions WHERE Spieler = '%1s' AND Sessionnumber = %2d;";
	private static final String SELECT_BYPLAYER_GESAMT = "SELECT Spieler, Punkte AS Punkte FROM Gesamt WHERE Spieler = '%1s';";

	public void erstelleTabelle(SqliteClient client) {
		try {
//			SqliteClient client = new SqliteClient("UnoDatenbank.sqlite");
			if (!client.tableExists("Sessions")) { // wenn die Tabelle bereits existiert, dann l�schen
				client.executeStatement(CREATETABLE); // erstelle Tabelle
			}

			if (!client.tableExists("Gesamt")) {
				client.executeStatement(CREATETABLE_Gesamt);
			}

		} catch (SQLException ex) {
			System.out.println("Ups! Something went wrong:" + ex.getMessage());
		}
	}

	public void InsertSession(String Spieler, int i, int sessionnumber, SqliteClient client) throws SQLException {
		client.executeStatement(String.format(INSERT_TEMPLATE_Sessions, Spieler, sessionnumber, i));
	}

	public ArrayList<HashMap<String, String>> ZwischenstandsAbfrage(SqliteClient client) throws SQLException {
		return client.executeQuery(SELECT_BYPLAYERANDSESSION);
	}

	public void InsertOrUpdateGesamt(String spieler, int score, int sessionnumber, SqliteClient client)
			throws SQLException {
		int h� = client.executeQuery(String.format("SELECT Spieler FROM Gesamt WHERE Spieler = '%1s'", spieler)).size(); // Spieler
																															// statt
																															// count
																															// Spieler
		boolean exists = h� > 0;

		if (exists) {
			UpdateGesamt(spieler, score, sessionnumber, client);

		} else {
			client.executeStatement(String.format(INSERT_TEMPLATE_Gesamt, spieler, score));

		}
	}

	private void UpdateGesamt(String Spieler, int i, int sessionnumber, SqliteClient client) throws SQLException {

		ArrayList<HashMap<String, String>> select = new ArrayList<>();

		int Zwischenstand = 0;

		select = client.executeQuery(String.format(SELECT_TEMPLATE_Gesamt, Spieler));
		HashMap<String, String> dings = select.get(0);
		String value = dings.get("Punkte");
		if (value != null) {
			Zwischenstand = Integer.valueOf(value);
		}
		Zwischenstand += i;
		client.executeStatement(String.format(Update_TEMPLATE_Gesamt, Zwischenstand, Spieler));
	}

	public int GetMaxSession(SqliteClient client) throws SQLException {
		ArrayList<HashMap<String, String>> result = client
				.executeQuery("SELECT MAX(Sessionnumber) AS Sessionnumber FROM Sessions");
		if (result.size() > 0) {
			String sessionNumber = result.get(0).get("Sessionnumber");
			if (sessionNumber != null) {
				return Integer.parseInt(sessionNumber);
			}
		}
		return 0;
	}

	public void Score(Spiel game, int Sessionnumber, SqliteClient client) throws SQLException {

		System.out.println("Nach der Runde " + Sessionnumber + " schaut der Punktestand folgenderma�en aus:");
		for (Spieler s : game.getMitspieler()) {

			ArrayList<HashMap<String, String>> Arrmap = client
					.executeQuery(String.format(SELECT_BYPLAYERANDSESSION, s.getName(), Sessionnumber));

			HashMap<String, String> map = Arrmap.get(0);

			String Spielername = map.get("Spieler");
			String punkte = map.get("Punkte");

			System.out.println(Spielername + " hat aktuell " + punkte + " Punkte");

		}

	}

	public void ScoreAll(Spiel game, SqliteClient client, int Sessionnumber) throws SQLException {
		HashMap<Integer, String> count = new HashMap<>();

		for (Spieler s : game.getMitspieler()) {
			ArrayList<HashMap<String, String>> Arrmap = client
					.executeQuery(String.format(SELECT_BYPLAYERANDSESSION, s.getName(), Sessionnumber));

			HashMap<String, String> map = Arrmap.get(0);

			String Spielername = map.get("Spieler");
			int punkte = Integer.parseInt(map.get("Punkte"));

			count.put(punkte, Spielername);

		}

		TreeMap<Integer, String> ts = new TreeMap<>();
		ts.putAll(count);

		System.out.println("Der Gewinner mit den wenigsten Punkten ist " + ts.get(ts.firstKey())
				+ ". Niedrigster Punktestand: " + ts.firstKey() + " Punkte");
		System.out.println("Der Verlierer mit den meisten Punkten ist " + ts.get(ts.lastKey())
				+ ". H�chster Punktestand: " + ts.lastKey() + " Punkte");

	}

	public void ScoreGesamt(Spiel game, SqliteClient client, String name) throws SQLException {

		ArrayList<HashMap<String, String>> Arrmap = client.executeQuery(String.format(SELECT_BYPLAYER_GESAMT, name));

		HashMap<String, String> map = Arrmap.get(0);

		String Spielername = map.get("Spieler");
		String punkte = map.get("Punkte");

		System.out.println(Spielername + " hat aktuell " + punkte + " Punkte");

	}

}
